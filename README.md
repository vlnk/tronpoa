**Laboratoire 3 : PJC**

Valentin LAURENT & Robin VERMES

Ce laboratoire consiste en l’implémentation d’un jeu **Light Tron Cycle**, qui est une course d’endurance entre 4 motos, chaque moto laissant une trace en se déplaçant, la trace laissée fait office de mur et si une moto entre en collision avec, elle est détruite. La dernière moto en service gagne la manche.

Le laboratoire nous a pris en tout une quarantaine d’heures, dont la moitié passée à débugger.

La méthode main instancie la classe *CCore*, qui sert de coeur à notre application. Elle initialise la partie graphique et les motos avec leurs IA, puis lorsque la manche est lancée, elle ordonne à la partie graphique de mettre à jour les déplacements des motos. La classe *CCore* contient les informations sur le plateau de jeu et permet aux motos de se diriger en conséquence.

Une fois le programme lancé, pour démarrer une manche il faut appuyer sur une TOUCHE QUELCONQUE.
Pour déplacer sa moto, le joueur doit utiliser les FLÈCHES DIRECTIONNELLES.
Pour stopper une manche, le joueur doit appuyer sur la BARRE ESPACE ou la touche ENTRÉE.
Pour relancer une manche, il doit encore une fois appuyer sur une TOUCHE QUELCONQUE.

**Description sommaire des IA** : 

+ *NoBadWay* cherche la direction qui donne accès au plus grand nombre de cases non encore marquées par une trace.

+ *ComeCloser* cherche à se rapprocher du joueur, cependant, s’il est trop près, elle cherche à s’éloigner un peu pour éviter de se faire piéger, et pour avoir la possibilité de barrer la route au joueur.

+ *Stupide* fait des déplacements aléatoires en privilégiant (lors du choix aléatoire) la dernière direction prise.


**Commentaires** :

Une grande partie du temps investi a été dépensé dans la recherche d’une solution à un problème lié au threads : il arrive que lorsqu’on redémarre une partie, une des IA soit morte directement, ou que tous les threads passent en état d’attente, ce qui bloque l’application. Aucune solution n’a été trouvée, nous avons pensé qu’il s’agissait d’un DEAD LOCK, et avons utilisé les outils de gestion des threads pour essayer d’y remédier (synchronized + wait + notifyAll puis les locks), mais cela n’a pas changé (bien que cela aura au moins permis de mieux comprendre le fonctionnement de ces outils).