package tron;

import tron.controller.*;
import tron.gui.CGameGUI;
import tron.gui.CFrameEcran;

import javax.swing.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

public class CCore {

    private ArrayList<AController> mControllers = new ArrayList<AController>();
    private ArrayList<CTron> mVehicules = new ArrayList<CTron>();

    private boolean[][] mArena;
    private final int PIXEL_SIZE = 10;
    private final int ARENA_SIZE = 65;

    private CGameGUI mGame;
    private final JFrame mFrame;
    private AController.DIRECTIONS mInput = AController.DIRECTIONS.LEFT;

    public enum GAME_STATE {READY, STARTED, FINISHED}
    private GAME_STATE mState = GAME_STATE.READY;

    private int mDeadTron = 0;
    private int mThreadsNumberInExec = 0;

    // CONSTRUCTEUR
    CCore() {
        mArena = new boolean[ARENA_SIZE][ARENA_SIZE];

        mState = GAME_STATE.READY;

        ImageIcon backgroundIcon = makeImageFromSizeAndColor(new Color(57,57,57));
        ImageIcon titleIcon = makeImageFromSizeAndColor(Color.GRAY);
        mGame = new CGameGUI(ARENA_SIZE, ARENA_SIZE, backgroundIcon, titleIcon);

        mFrame = new CFrameEcran(mGame, ARENA_SIZE * PIXEL_SIZE, ARENA_SIZE * PIXEL_SIZE);
        mFrame.setBackground(new Color(72, 76, 78));

        mGame.setFocusable(true);
        mFrame.addKeyListener(new KeyListener() {
                    @Override
                    public void keyTyped(KeyEvent e) {}

                    @Override
                    public void keyPressed(KeyEvent e) {
                        int keyCode = e.getKeyCode();

                        if (mState == GAME_STATE.STARTED) {
                            switch (keyCode) {
                                case KeyEvent.VK_UP:
                                    mInput = AController.DIRECTIONS.UP;
                                    break;
                                case KeyEvent.VK_DOWN:
                                    mInput = AController.DIRECTIONS.DOWN;
                                    break;
                                case KeyEvent.VK_LEFT:
                                    mInput = AController.DIRECTIONS.LEFT;
                                    break;
                                case KeyEvent.VK_RIGHT:
                                    mInput = AController.DIRECTIONS.RIGHT;
                                    break;
                                case KeyEvent.VK_ENTER:
                                    mState = GAME_STATE.FINISHED;
                                    break;
                                case KeyEvent.VK_SPACE:
                                    mState = GAME_STATE.FINISHED;
                                    break;
                                default:
                                    break;
                            }
                        }
                        else {
                            if (mState == GAME_STATE.READY) {
                                mState = GAME_STATE.STARTED;
                            }
                            else if (mState == GAME_STATE.FINISHED) {
                                reload();
                            }
                        }
                    }

                    @Override
                    public void keyReleased(KeyEvent e) {}
        });

        mFrame.setFocusable(true);

        mFrame.pack();
        mFrame.setVisible(true);

        mGame.updateConsole("Appuyez sur <ESPACE>...");

        initialize();
    }

    // GETTERS & SETTERS
    public int getHauteur() { return ARENA_SIZE; }
    public int getLargeur() { return ARENA_SIZE; }

    public AController.DIRECTIONS getInputDirection() { return mInput; }
    synchronized public GAME_STATE getState() { return mState; }
    public void addDead() { mDeadTron++; }

    synchronized public int getThreadNbInExec() { return mThreadsNumberInExec; }

    // METHODES DE LOCALISATION
    synchronized public boolean isBlocked(int x, int y) {
        boolean blocked = false;

        if (x >= ARENA_SIZE || y >= ARENA_SIZE ||
        		x < 0 || y < 0){
            blocked = true;
        }
        else if (mArena[x][y]){
            blocked = true;
        }

        return blocked;
    }
    synchronized public boolean updatePosition(int posX, int posY, Icon skin) {
        boolean isCorrectPosition = true;

        try {
            mGame.setIcon(posX, posY, skin);
            mArena[posX][posY] = true;
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("X:" + posX + ", Y:" + posY);
            isCorrectPosition = false;
        }

        return isCorrectPosition;
    }

    // METHODES DE CHARGEMENT DU JEU
    public void initialize() {
        // initialisation du jeu
        for (boolean[] line : mArena)
            Arrays.fill(line, false);

        //ArrayList<AController> controllers = new ArrayList<AController>();

        // initialisation des joueurs
        Point positionPlayer = new Point(ARENA_SIZE/2, ARENA_SIZE/5);
        Point positionComeCloser = new Point(ARENA_SIZE / 2, ARENA_SIZE - (ARENA_SIZE / 5));
        Point positionNoBadWay = new Point(ARENA_SIZE / 4, ARENA_SIZE / 3);
        Point positionStupide = new Point(ARENA_SIZE / 6, ARENA_SIZE / 2);

        CPlayer mJoueur = new CPlayer(this, positionPlayer, new Color(175, 248, 245), "Player");

        mControllers.clear();
        mControllers.add(mJoueur);
        mControllers.add(new CIAComeCloser(this, positionComeCloser, new Color(251, 218, 76), mJoueur, "IA1"));
        mControllers.add(new CIAStupide(this, positionStupide, new Color(189, 81, 15), "IA2"));
        mControllers.add(new CIANoBadWay(this, positionNoBadWay, new Color(255, 184, 68), "IA3"));

        // initialisation des threads
        mVehicules.clear();

 /*       for (AController controller : controllers) {
            controller.setInitializedPosition();
            mVehicules.add(new CTron(this, controller));
        }
 */       
        
        for (int i = 0;i < mControllers.size(); i++){
        	mControllers.get(i).setInitializedPosition();
            mVehicules.add(new CTron(this, mControllers.get(i), i));
        }
    }

    public void reload() {
    	mDeadTron = 0;
    	mThreadsNumberInExec = 0;

        // initialisation du jeu
        for (boolean[] line : mArena)
            Arrays.fill(line, false);

        mState = GAME_STATE.READY;
        
        mGame.reloadGUI();
        
        mFrame.revalidate();
        mFrame.repaint();
    }

    synchronized public void updateTronStatus(CTron tron) {

        if (mState == GAME_STATE.READY) {
            mGame.updateTronStatus(tron.getController(), "PRÊT");
            mGame.updateConsole("Appuyez sur <ESPACE> pour commencer la partie.");

        } else if (mState == GAME_STATE.STARTED) {

            mGame.updateConsole(String.valueOf(mDeadTron));

            if (!tron.getController().isDead()) {
                mGame.updateTronStatus(tron.getController(), "ACTIF");
            } else {
                mGame.updateTronStatus(tron.getController(), "MORT");
            }
        }


        if (mState == GAME_STATE.STARTED && mDeadTron == mVehicules.size() - 1) {
            if (!tron.getController().isDead()) {
                mGame.updateConsole(tron.getController() + " a gagné !");
                tron.getController().win();
                //finish();
                mState = GAME_STATE.FINISHED;
                mGame.updateTronStatus(tron.getController(), "WINNER");
            }
        }

        mFrame.revalidate();
        mFrame.repaint();
    }

    public void finish() {
        mState = GAME_STATE.FINISHED;
        mVehicules.clear();

        for (int i = 0;i < mControllers.size(); i++){
            mControllers.get(i).setInitializedPosition();
            mVehicules.add(new CTron(this, mControllers.get(i), i));
        }
    }

    // METHODES D'INITIALISATION (CREATION DES ICONS)
    public ImageIcon makeImageFromSizeAndColor(int size, Color color) {
        BufferedImage img = new BufferedImage( size, size, BufferedImage.TYPE_INT_RGB );

        // Get a Graphics object
        Graphics g = img.getGraphics();

        // Create white background
        g.setColor(color);
        g.fillRect(0, 0, size, size);

        return new ImageIcon(img);
    }
    public ImageIcon makeImageFromSizeAndColor(Color color) {
        return makeImageFromSizeAndColor(PIXEL_SIZE, color);
    }
    
    // METHODE DE GESTION DE LA SYNCHRONISATION DES THREADS
    synchronized public void moveToNextThread(){
    	mThreadsNumberInExec = (mThreadsNumberInExec + 1) % mVehicules.size();
    }
}
