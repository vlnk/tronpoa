package tron;

import tron.controller.AController;

import java.awt.*;

public class CTron extends Thread {
    private final CCore mCore;
    private AController mController;
    private volatile boolean stopRequested = false;
    private int Threadnum;
    
    CTron(CCore core, AController controller, int num){
        mCore = core;
        mController = controller;
        mCore.updatePosition(mController.getPosition().x, mController.getPosition().y, mController.getIcon());
        Threadnum = num;
        start();
    }

    public AController getController() { return mController; }
    
    @Override
    public void run() {

        while (!stopRequested) {

            synchronized (mCore) {
                while (Threadnum != mCore.getThreadNbInExec()) {
                    try {
                        mCore.wait();
                    } catch (InterruptedException e) {
                        System.out.println("CTron Class : wait failed");
                        e.printStackTrace();
                    }
                }

                if (mCore.getState() == CCore.GAME_STATE.STARTED) {

                    if (!mController.updateMove())
                        mController.die();

                    Point nextPosition = mController.getPosition();

                    if (mCore.isBlocked(nextPosition.x, nextPosition.y)) {
                        mController.die();
                    }

                    // output
                    if (!mController.isDead()) {
                        mCore.updatePosition(nextPosition.x, nextPosition.y, mController.getIcon());
                    } else {
                        System.out.println("Dead");
                    }


                } else if (mCore.getState() == CCore.GAME_STATE.READY) {
                    mController.live();

                    Point initialPosition = mController.getPosition();
                    mCore.updatePosition(initialPosition.x, initialPosition.y, mController.getIcon());
                }

                //yield();
                mCore.updateTronStatus(this);

                //donner la main au thread suivant
                mCore.moveToNextThread();

                mCore.notifyAll();
            }

            try {
                sleep(50);
            } catch (InterruptedException e) {
                System.out.println(mController + "Tron has failed !");
                stopRequested = true;
            }
        }
    }

    @Override
    public String toString() {
        return mController + " : " +  mController.isDead();
    }
}
