package tron.gui;

import javax.swing.*;
import java.awt.*;

public abstract class ATronPanelGUI extends JPanel {

    public void initialize(LayoutManager layout, Dimension dimension) {
        setLayout(layout);

        setAlignmentX(Component.LEFT_ALIGNMENT);
        setAlignmentY(Component.CENTER_ALIGNMENT);
        setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        setOpaque(false);

        setPreferredSize(dimension);
    }

    public JLabel makeConsoleLabel(String text, Color fg, Color bg) {
        JLabel info = new JLabel();

        Font font = new Font("Monaco", Font.BOLD, 12);
        info.setFont(font);
        info.setForeground(fg);
        info.setBackground(bg);
        info.setPreferredSize(new Dimension(150, font.getSize() + 5));

        info.setAlignmentX(Component.LEFT_ALIGNMENT);
        info.setAlignmentY(Component.CENTER_ALIGNMENT);
        info.setVerticalAlignment(SwingConstants.CENTER);
        info.setVerticalTextPosition(JLabel.CENTER);

        if (text != null)
            info.setText(text);

        return info;
    }
}

