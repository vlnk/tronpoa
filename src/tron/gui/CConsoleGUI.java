package tron.gui;

import javax.swing.*;
import java.awt.*;

public class CConsoleGUI extends ATronPanelGUI {

    JLabel mInfo = null;

    public CConsoleGUI() {
        initialize(new BoxLayout(this, BoxLayout.PAGE_AXIS), new Dimension(200, 20));
    }

    void updateInfo(String info) {
        if (mInfo == null) {
            mInfo = makeConsoleLabel(null, Color.WHITE, new Color(72, 76, 78));
            mInfo.setPreferredSize(new Dimension(200, 20));
            mInfo.setHorizontalTextPosition(JLabel.CENTER);
            add(mInfo);
        }

        mInfo.setText(info);
    }
}
