package tron.gui;

import tron.controller.AController;

import javax.swing.*;

public class CGameGUI extends JComponent {
    private static final long serialVersionUID = 1L;
    
    private CConsoleGUI mConsole;
    private CTronStatusGUI mTronStatus;
    private CArenaGUI mArenaGUI;

    //public CEcranGUI(){ this (50, 10, 80, 10, 12, null); }
    //public CEcranGUI(int nbrLignes, int hauteur){ this (nbrLignes, 10, hauteur, 10, 12, null); }
    //public CGameGUI(int nbrLignes, int nbrColonnes, ImageIcon fond){ this (nbrLignes, 10, nbrColonnes, 10, 8, fond); }
    //public CEcranGUI(int nbrLignes, int hauteur, int size){ this (nbrLignes, 10, hauteur, 10, size, null); }

    /** Le constructeur complet. */
    //public CGameGUI(int nbrLignes, int hauteur, int nbrColonnes, int largeur, int sizeText, ImageIcon fond) {
    public CGameGUI(int nbrLignes, int nbrColonnes, ImageIcon fond, ImageIcon title) {

        mConsole = new CConsoleGUI();
        mTronStatus = new CTronStatusGUI();
        mArenaGUI = new CArenaGUI(nbrLignes, nbrColonnes, fond, title);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        add(mConsole);
        add(mArenaGUI);
        add(mTronStatus);
    }

    public boolean setIcon (int Lig, int Col, Icon Im){
        return mArenaGUI.setIcon(Lig, Col, Im);
    }

    public void reloadGUI() { mArenaGUI.reloadGUI(); }

    public void updateConsole(String info) { mConsole.updateInfo(info); }
    public void updateTronStatus(AController controller, String text) {
        mTronStatus.updateControllerInfo(controller, text);
    }
}



