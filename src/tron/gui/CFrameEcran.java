package tron.gui;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class CFrameEcran extends JFrame {
    private static final long serialVersionUID = 1L;
    private CGameGUI mEcranGUI;

    public CFrameEcran(CGameGUI ecranGUI, int Lar, int Hau) {
        super ("Tron Game");
        mEcranGUI = ecranGUI;
        
        setSize(Lar, Hau);
        setContentPane(mEcranGUI);

        addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                System.out.println("Fin");
                System.exit(1);
            }});
    }
}

