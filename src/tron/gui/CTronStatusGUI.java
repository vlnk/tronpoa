package tron.gui;

import tron.controller.AController;

import javax.swing.*;
import java.awt.*;
import java.util.Hashtable;

public class CTronStatusGUI extends ATronPanelGUI {

    Hashtable<AController, JLabel> mDictionaryInfo = new Hashtable<AController, JLabel>();

    public CTronStatusGUI() {

        initialize(new FlowLayout(FlowLayout.LEFT, 0, 0), new Dimension(200, 20));
        //add(makeConsoleLabel("CONTRÔLES : Appuyez sur <ENTER> pour commencer et recharger."));
    }

    public void updateControllerInfo(AController controller, String text) {

        if (!mDictionaryInfo.containsKey(controller)) {
            JLabel label = makeConsoleLabel(null, controller.getColor(), new Color(72, 76, 78));

            mDictionaryInfo.put(controller, label);
            add(label);
        }

        mDictionaryInfo.get(controller).setText(controller + "(" + controller.getScore() + ") : " + text);
    }
}
