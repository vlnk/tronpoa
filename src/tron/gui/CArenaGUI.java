package tron.gui;

import javax.swing.*;
import java.awt.*;

public class CArenaGUI extends JPanel {

    private JLabel[][] mGrid;

    private int mNbLignes;
    private int mNbColonnes;

    private ImageIcon mBackground;
    private ImageIcon mTitleIcon;

    public CArenaGUI(int nbRows, int nbColumns, ImageIcon background, ImageIcon titleIcon) {
        mNbLignes = nbRows;
        mNbColonnes = nbColumns;
        mBackground = background;
        mTitleIcon = titleIcon;

        mGrid = new JLabel[mNbLignes][mNbColonnes];

        setLayout(new GridLayout(mNbLignes, mNbColonnes, 0, 0));
        setAlignmentX(Component.LEFT_ALIGNMENT);

        for (int y = 0; y < mGrid.length; y++) {
            for (int x = 0; x < mGrid[y].length; x++) {

                mGrid[y][x] = new JLabel(mBackground);

                add(mGrid[y][x]);
                mGrid[y][x].setBorder(BorderFactory.createEmptyBorder());
            }
        }

        setTitle();
    }

    public boolean setIcon (int Lig, int Col, Icon Im){
        if (Lig >= mGrid.length || Lig < 0) return false;
        if (Col >= mGrid[Lig].length || Col< 0) return false;

        mGrid[Col][Lig].setIcon(Im);
        return true;
    }

    public void reloadGUI() {
        for (JLabel[] aMGrid : mGrid) {
            for (JLabel label : aMGrid) {
                label.setIcon(mBackground);
            }
        }

        setTitle();
    }

    private void setTitle() {
        Point[] title = new Point[]
                {
                        //T
                        new Point(23, 23), new Point(24, 23), new Point(25, 23), new Point(26, 23),
                        new Point(23, 24), new Point(24, 24), new Point(25, 24), new Point(26, 24),
                        new Point(24, 25), new Point(25, 25),
                        new Point(24, 26), new Point(25, 26),
                        new Point(24, 27), new Point(25, 27),
                        new Point(24, 28), new Point(25, 28),

                        //R
                        new Point(28, 23), new Point(29, 23), new Point(30, 23), new Point(31, 23),
                        new Point(28, 24), new Point(31, 24),
                        new Point(28, 25), new Point(29, 25), new Point(30, 25), new Point(31, 25),
                        new Point(28, 26), new Point(29, 26),
                        new Point(28, 27), new Point(30, 27),
                        new Point(28, 28), new Point(31, 28),

                        //O
                        new Point(33, 23), new Point(34, 23), new Point(35, 23), new Point(36, 23),
                        new Point(33, 24), new Point(36, 24),
                        new Point(33, 25), new Point(36, 25),
                        new Point(33, 26), new Point(36, 26),
                        new Point(33, 27), new Point(36, 27),
                        new Point(33, 28), new Point(34, 28), new Point(35, 28), new Point(36, 28),

                        //N
                        new Point(38, 23), new Point(41, 23),
                        new Point(38, 24), new Point(39, 24), new Point(41, 24),
                        new Point(38, 25), new Point(39, 25), new Point(41, 25),
                        new Point(38, 26), new Point(40, 26), new Point(41, 26),
                        new Point(38, 27), new Point(40, 27), new Point(41, 27),
                        new Point(38, 28), new Point(41, 28)
                };

        for (Point px : title) {
            setIcon(px.x, px.y, mTitleIcon);
        }

    }
}
