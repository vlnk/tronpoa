package tron.controller;

import tron.CCore;

import javax.swing.*;

import java.awt.*;
import java.util.Random;

public abstract class AController {
    public enum DIRECTIONS {UP, RIGHT, DOWN, LEFT}

    protected Color mColor;
    protected ImageIcon mIcon;

    protected CCore mCore;
    protected boolean mDead = true;

    protected String mName;
    protected int nbCol;
	protected int nbLig;
    protected int mScore = 0;

    private Point mInitialPosition = null;
    protected Point mPosition = new Point();
    protected DIRECTIONS mMove = DIRECTIONS.RIGHT;
    protected DIRECTIONS mLastDirection = DIRECTIONS.RIGHT;

    // constructeur
    AController(CCore core, Point position, Color color, String name) {
        mCore = core;
        mName = name;
        mColor = color;
        mIcon = mCore.makeImageFromSizeAndColor(color);

        mInitialPosition = position;
    }
    
    public final Point getPosition() { return mPosition; }

    public final boolean isDead() { return mDead; }

    public final void die() {
        if (!mDead) {
            mDead = true;
            mCore.addDead();
        }
    }

    public final void live() {
        setInitializedPosition();
        mDead = false;
    }

    public final void win() { mScore++; }

    public final ImageIcon getIcon() { return mIcon; }

    //aleatoire IA par défaut
  	//0 = gauche
  	//1 = droite
  	//2 = haut
  	//3 = bas
  	public boolean updateMove() {
        if (isDead()) return true;

        Random des = new Random();
        int myRand;
        boolean fait = false;

        while(!fait){
            myRand = des.nextInt(25);

            switch(myRand){
                case 0 :
                    fait = choixDirection(DIRECTIONS.LEFT);
                    break;
                case 1 :
                    fait = choixDirection(DIRECTIONS.RIGHT);
                    break;
                case 2 :
                    fait = choixDirection(DIRECTIONS.UP);
                    break;
                case 3 :
                    fait = choixDirection(DIRECTIONS.DOWN);
                    break;
                default : //plus de chances de continuer
                    fait = choixDirection(mLastDirection);
                    break;
            }
        }

        return true;
      }
    
  	//méthode pour verifier une direction et faire le mouvement si possible
  	protected boolean choixDirection(DIRECTIONS myDirection){
		boolean fait = false;
		
		if (mCore.isBlocked(getPosition().x - 1, getPosition().y) &&
	              mCore.isBlocked(getPosition().x + 1, getPosition().y) &&
	              mCore.isBlocked(getPosition().x, getPosition().y-1) &&
	              mCore.isBlocked(getPosition().x, getPosition().y+1)) {

            fait = true;
            getPosition().y--;
        }
		
		try{
			switch(myDirection){
			case LEFT :
				if (getPosition().x != 0 && mLastDirection != DIRECTIONS.RIGHT &&
	                !mCore.isBlocked(getPosition().x-1, getPosition().y)) {
					getPosition().x--;
					fait = true;
					mLastDirection = DIRECTIONS.LEFT;
				}
				break;
			case RIGHT :
				if (getPosition().x != nbCol - 1 && mLastDirection != DIRECTIONS.LEFT &&
	                !mCore.isBlocked(getPosition().x + 1, getPosition().y)) {
					getPosition().x++;
					fait = true;
					mLastDirection = DIRECTIONS.RIGHT;
				}
				break;
			case UP :
				if (getPosition().y != 0 && mLastDirection != DIRECTIONS.DOWN &&
	                !mCore.isBlocked(getPosition().x, getPosition().y - 1)) {
					getPosition().y--;
					fait = true;
					mLastDirection = DIRECTIONS.UP;
				}
				break;
			case DOWN :
				if (getPosition().y != nbLig - 1 && mLastDirection != DIRECTIONS.UP &&
	                !mCore.isBlocked(getPosition().x, getPosition().y+1)) {
					getPosition().y++;
					fait = true;
					mLastDirection = DIRECTIONS.DOWN;
				}
				break;
			}
		}
		catch(NullPointerException e){ //une IA qui ne trouve plus de direction
			fait = true;
			this.getPosition().x++;
			//on la tue
		}
		
		return fait;
	}

    public void setInitializedPosition() {

        mPosition.setLocation(mInitialPosition.getX(), mInitialPosition.getY());
    }
    public Color getColor() { return mColor; }
    public int getScore() { return mScore; }

    @Override
    public final String toString() { return mName; }
}
