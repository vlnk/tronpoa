package tron.controller;

import java.awt.Color;
import java.awt.Point;
import java.util.Arrays;
import java.util.Random;
import java.lang.Math;

import tron.CCore;

public class CIAComeCloser extends AController {

	private CPlayer mJoueur;
	
	public CIAComeCloser(CCore core, Point position, Color color, CPlayer Joueur, String name){
		super(core, position, color, name);

		mJoueur = Joueur;
		nbLig = mCore.getHauteur();
		nbCol = mCore.getLargeur();
        mMove = DIRECTIONS.UP;
        mLastDirection = DIRECTIONS.UP;
	}
	
	public boolean updateMove() {
        boolean fait;
		if (isDead()) return false;

		mMove = directionProche(this.getPosition(), mJoueur.getPosition());
		fait  = choixDirection(mMove);
		
        return fait;
    }
	
	private DIRECTIONS directionProche(Point maPosition, Point posJoueur){
		DIRECTIONS[] tabRand = new DIRECTIONS[20];
		Random Des = new Random();
		int myRand;
		int nbDirection = 0;
		int x;
		int y;
		int[] resultDirs = new int[4];
		int bestDistance = nbLig*nbCol;
		int worstDistance = 0; // sert dans le cas ou on est trop proche du joueur
							   // si on essaie de s'eloigner et que ca n'est pas possible
							   //worstDistance nous permet de prendre le chemin 
							   //qui se rapproche quand meme
		
		
		//direction UP
		x = maPosition.x;
		y = maPosition.y - 1;
		resultDirs[0] = calculeDistance(x, y, posJoueur);
		if (resultDirs[0] < bestDistance){
			bestDistance = resultDirs[0];
		}
		if (resultDirs[0] > worstDistance && resultDirs[0] != nbLig*nbCol){
			worstDistance = resultDirs[0];
		}
		
		//direction DOWN
		x = maPosition.x;
		y = maPosition.y + 1;
		resultDirs[1] = calculeDistance(x, y, posJoueur);
		if (resultDirs[1] < bestDistance){
			bestDistance = resultDirs[1];
		}
		if (resultDirs[1] > worstDistance && resultDirs[1] != nbLig*nbCol){
			worstDistance = resultDirs[1];
		}
		
		//direction LEFT
		x = maPosition.x - 1;
		y = maPosition.y;
		resultDirs[2] = calculeDistance(x, y, posJoueur);
		if (resultDirs[2] < bestDistance){
			bestDistance = resultDirs[2];	
		}
		if (resultDirs[2] > worstDistance && resultDirs[2] != nbLig*nbCol){
			worstDistance = resultDirs[2];
		}
		
		//direction RIGHT
		x = maPosition.x + 1;
		y = maPosition.y;
		resultDirs[3] = calculeDistance(x, y, posJoueur);
		if (resultDirs[3] < bestDistance){
			bestDistance = resultDirs[3];
		}
		if (resultDirs[3] > worstDistance && resultDirs[3] != nbLig*nbCol){
			worstDistance = resultDirs[3];
		}
		
		//System.out.println("********best = " + bestDistance + " - worst = " + worstDistance);
		if (bestDistance > 1 || worstDistance <= 1){ //si on est loin on veut se rapprocher
			for (int i = 0; i < 4; i++){//on place un peu d'aleatoire			
				//System.out.println(resultDirs[i]);
				if (bestDistance - resultDirs[i] == 0){
					switch (i){
						case 0 : 
							tabRand[nbDirection] = DIRECTIONS.UP;
							break;
						case 1 : 
							tabRand[nbDirection] = DIRECTIONS.DOWN;
							break;
						case 2 : 
							tabRand[nbDirection] = DIRECTIONS.LEFT;
							break;
						case 3 : 
							tabRand[nbDirection] = DIRECTIONS.RIGHT;
							break;
					}
					nbDirection++;
				}
			}
		}
		else{//si on est pres, on ne veut pas rester trop pres, 
			//pour pouvoir lui mettre un mur dans la face
			for (int i = 0; i < 4; i++){//on place un peu d'aleatoire			
				//System.out.println(resultDirs[i]);
				if (worstDistance - resultDirs[i] == 0){
					switch (i){
						case 0 : 
							tabRand[nbDirection] = DIRECTIONS.UP;
							break;
						case 1 : 
							tabRand[nbDirection] = DIRECTIONS.DOWN;
							break;
						case 2 : 
							tabRand[nbDirection] = DIRECTIONS.LEFT;
							break;
						case 3 : 
							tabRand[nbDirection] = DIRECTIONS.RIGHT;
							break;
					}
					nbDirection++;
				}
			}
		}
		
		if (nbDirection >= 2){
			boolean estdans = false;
			//on regarde si la derniere direction est dans une de celles choisies pour la privilegier
			for (int idx = 0; idx < nbDirection; idx++){
				if (tabRand[idx] == mLastDirection){
					estdans = true;
				}
			}
			if (estdans){
				while(nbDirection < 20){
					tabRand[nbDirection] = mLastDirection;
					nbDirection++;
				}
			}
			myRand = Des.nextInt(nbDirection);	

		}
		else{
			myRand = 0;
		}
	
		return tabRand[myRand];
	}
	
	
	private int calculeDistance(int x, int y, Point posJoueur){
		int ret;
		int calculx;
		int calculy;
		int[][] visited = new int[nbLig][nbCol];
		for (int[] line : visited) // initialise les cases à non visitée
            Arrays.fill(line, 0);

		if (!mCore.isBlocked(x, y)){
			calculx = Math.abs(x - posJoueur.x);
			calculy = Math.abs(y - posJoueur.y);
			ret = calculx + calculy;
			
			// penalise pres du bord
			if (x == 0 || y == 0 || x == nbLig - 1 || y == nbCol -1){
				ret += 4; 
			}
			// penalise les culs de sac
			if (nbCaseProche(x, y, visited, 0) < 15){
				ret += 4;
			}
		}
		else ret = nbLig*nbCol;
		return ret;
	}
	
	//int deep pour eviter le stackoverflow (profondeur de 1024 appels en crée un)
	private int nbCaseProche(int x, int y, int[][] visited, int deep){
		int nb = 0;
		
		if (!mCore.isBlocked(x, y)){
			nb++;
			visited[x][y] = 1;
			if (deep < 1023){
				if ((x + 1) < nbLig && visited[x + 1][y] == 0){
					nb += nbCaseProche(x + 1, y, visited, deep+1);
				}
				if (x > 0 && visited[x - 1][y] == 0){
					nb += nbCaseProche(x - 1, y, visited, deep+1);
				}
				if ((y + 1) < nbCol && visited[x][y + 1] == 0){
					nb += nbCaseProche(x, y + 1, visited, deep+1);
				}
				if (y > 0 && visited[x][y - 1] == 0){
					nb += nbCaseProche(x, y - 1, visited, deep+1);
				}
			}
			
		}
		
		return nb;
	}
}
