package tron.controller;

import java.awt.*;
import java.util.Random;

import tron.CCore;

public class CIAFollow extends AController {

	public CIAFollow(CCore core, Point position, Color color, String name){
		super(core, position,color, name);
		
		nbLig = mCore.getHauteur();
		nbCol = mCore.getLargeur();
        mMove = DIRECTIONS.LEFT;
	}
	
	@Override
	//IA qui fait l'inverse du joueur
	//si elle ne peut pas elle continue et elle attend de pouvoir le faire
	//si elle ne peut pas non plus elle fait un random
	public boolean updateMove() {
        if (isDead()) return false;

        switch (mCore.getInputDirection()) {
            case UP:
                mMove = DIRECTIONS.DOWN;
                break;
            case DOWN:
                mMove = DIRECTIONS.UP;
                break;
            case LEFT:
                mMove = DIRECTIONS.RIGHT;
                break;
            case RIGHT:
                mMove = DIRECTIONS.LEFT;
                break;
            default:
                break;
        }

		boolean fait = false;
		int myRand;
		Random alea = new Random();
		myRand = alea.nextInt(100);
    	
		if (myRand < 95){ // si pas aleatoire
			fait = choixDirection(mMove);
			
			if (!fait){ // on essaye de continuer le dernier mouvement
	    		fait = choixDirection(mLastDirection);
	    	}
		}

    	// sinon
    	if (!fait || myRand >= 95){ //on utilise la méthode aléatoire de l'IA stupide (par défaut)
    		super.updateMove();
    	}

        return true;
    }
	
}
