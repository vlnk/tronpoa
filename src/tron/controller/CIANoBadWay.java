package tron.controller;

import java.awt.*;
import java.util.Random;

import tron.CCore;

public class CIANoBadWay extends AController {
	
	public CIANoBadWay(CCore core, Point position, Color color, String name){
		super(core, position, color, name);

		nbLig = mCore.getHauteur();
		nbCol = mCore.getLargeur();
        mMove = DIRECTIONS.UP;
        mLastDirection = DIRECTIONS.UP;
	}
	
	@Override
	//IA qui regarde a chaque fois quelle est la direction qui donne acces au plus grand nombre de case
	public boolean updateMove() {
        boolean fait = false;
		if (isDead()) return false;

		while(!fait){	
			mMove = meilleureDirection(this.getPosition());
			fait  = choixDirection(mMove);
		}
        return true;
    }
	
	private DIRECTIONS meilleureDirection(Point position){
		int exploredDirections = 0;

		int[] numOfAccessibleCases = new int[4];
		
		int bestNbAcces = 0;
		int x = position.x;
		int y = position.y - 1;
		
		Random Des = new Random();
		int myRand;
		
		DIRECTIONS[] tabRand = new DIRECTIONS[20];

		while(exploredDirections < 4){

			numOfAccessibleCases[exploredDirections] = nbCaseDispo(x, y);

			if (numOfAccessibleCases[exploredDirections] > bestNbAcces){
				bestNbAcces = numOfAccessibleCases[exploredDirections];
			}
			switch (exploredDirections) { //explore next direction
				case 0 : 
					x = position.x;
					y = position.y + 1;
					break;
				case 1 :
					x = position.x - 1;
					y = position.y;
					break;
				case 2 :
					x = position.x + 1;
					y = position.y;
					break;
				default :
					break;
			}

			exploredDirections++;
			
		}

		// rempli le tableaux avec les meilleurs directions trouvées
		int compteuralea = fillWithBestDirections(tabRand, bestNbAcces, numOfAccessibleCases);
		
		if (compteuralea >= 2){
			boolean estdans = false;
			//on regarde si la derniere direction est dans une de celles choisies pour la privilegier
			for (int idx = 0; idx < compteuralea; idx++){
				if (tabRand[idx] == mLastDirection){
					estdans = true;
				}
			}
			if (estdans){
				while(compteuralea < 20){
					tabRand[compteuralea] = mLastDirection;
					compteuralea++;
				}
			}
			myRand = Des.nextInt(compteuralea);	

		}
		else{
			myRand = 0;
		}
	
		return tabRand[myRand];
	}
	
	private int fillWithBestDirections(DIRECTIONS[] tableOfDirections, int bestNbAcces, int[] numOfAccessibleCases) {
		int counterFilling = 0;

		//on place un peu d'aleatoire
		for (int i = 0; i < 4; i++) {

			if (bestNbAcces - numOfAccessibleCases[i] == 0) {
				switch (i){
					case 0 :
						tableOfDirections[counterFilling] = DIRECTIONS.UP;
						break;
					case 1 :
						tableOfDirections[counterFilling] = DIRECTIONS.DOWN;
						break;
					case 2 :
						tableOfDirections[counterFilling] = DIRECTIONS.LEFT;
						break;
					case 3 :
						tableOfDirections[counterFilling] = DIRECTIONS.RIGHT;
						break;
				}

				counterFilling++;
			}
		}

		return counterFilling;
	}


	//met a jour le nombre de cases accessibles pour un bloc
	// la case d'entree doit toujours etre accessible
	private boolean nbCaseBloc(int x, int y, int[][] blocs, int bordureXinf, int bordureXsup, 
			int bordureYinf, int bordureYsup, int[][] visited, int bloclig, int bloccol){
		boolean ret = false;
		boolean riendeplus = false;
		
		visited[x][y] = 2;
		
		//on traite ensuite chaque direction
		//à gauche
		if(x - 1 >= 0){
			if (!mCore.isBlocked(x - 1, y) && visited[x-1][y] == 0){
				if (x - 1 < bordureXinf){ // si dans un autre bloc et que la case est accessible
					riendeplus = true;
					visited[x - 1][y] = 1;
					blocs[bloclig - 1][bloccol]++;
				}
				else{
					blocs[bloclig][bloccol]++;
					riendeplus = nbCaseBloc(x-1, y, blocs, bordureXinf, bordureXsup, 
							bordureYinf, bordureYsup, visited, bloclig, bloccol);
				}
				if (riendeplus){
					ret = true;
				}
			}
		}
		//à droite
		if(x + 1 < nbLig){
			if (!mCore.isBlocked(x + 1, y) && visited[x+1][y] == 0){
				if (x + 1 > bordureXsup){ // si dans un autre bloc et que la case est accessible
					riendeplus = true;
					visited[x + 1][y] = 1;
					blocs[bloclig + 1][bloccol]++;
				}
				else{
					blocs[bloclig][bloccol]++;
					riendeplus = nbCaseBloc(x+1, y, blocs, bordureXinf, bordureXsup, 
							bordureYinf, bordureYsup, visited, bloclig, bloccol);
				}
				if (riendeplus){
					ret = true;
				}
			}
		}
		//au dessus
		if(y - 1 >= 0){
			if (!mCore.isBlocked(x, y - 1) && visited[x][y - 1] == 0){
				if (y - 1 < bordureYinf){ // si dans un autre bloc et que la case est accessible
					riendeplus = true;
					visited[x][y - 1] = 1;
					blocs[bloclig][bloccol - 1]++;
				}
				else{
					blocs[bloclig][bloccol]++;
					riendeplus = nbCaseBloc(x, y - 1, blocs, bordureXinf, bordureXsup, 
							bordureYinf, bordureYsup, visited, bloclig, bloccol);
				}
				if (riendeplus){
					ret = true;
				}
			}
		}
		//au dessus
		if(y + 1 < nbCol){
			if (!mCore.isBlocked(x, y + 1) && visited[x][y + 1] == 0){
				if (y + 1 > bordureYsup){ // si dans un autre bloc et que la case est accessible
					riendeplus = true;
					visited[x][y + 1] = 1;
					blocs[bloclig][bloccol + 1]++;
				}
				else{
					blocs[bloclig][bloccol]++;
					riendeplus = nbCaseBloc(x, y + 1, blocs, bordureXinf, bordureXsup, 
							bordureYinf, bordureYsup, visited, bloclig, bloccol);
				}
				if (riendeplus){
					ret = true;
				}
			}
		}
				
		return ret;
	}
	
	
	//parcours les cases d'entrée d'un bloc et lance une recherche pour chacune d'entre elles
	//(une case d'entree peut etre parcourue lors de la recherche a partir d'une autre case
	// et ainsi n'est plus eligible pour lancer une recherche)
	//retourne vrai si un bloc adjacent à obtenu un nouveau point d'entree
	private boolean visiteBloc(int[][] blocs, int xbloc, int ybloc, int[][]visited){
		boolean ret = false;
		//init des bordures du bloc
		int bordureXinf = (xbloc*(nbLig/4));
		int bordureXsup;
		int bordureYinf = (ybloc*(nbCol/4));
		int bordureYsup;

		if (xbloc == 3) bordureXsup = nbLig - 1;
		else bordureXsup = bordureXinf + nbLig/4;
		if (ybloc == 3) bordureYsup = nbCol - 1;
		else bordureYsup = bordureYinf + nbCol/4;
		
		for (int x = bordureXinf; x <= bordureXsup; x++){
			for (int y = bordureYinf; y <= bordureYsup; y++){
				if (visited[x][y] == 1){//si c'est un point d'entree
					ret = nbCaseBloc(x, y, blocs, bordureXinf, bordureXsup, 
							bordureYinf, bordureYsup, visited, xbloc, ybloc);
				}
			}
		}
		
		
		return ret;
	}
	
	
	//****
	//nbCaseDispo cherche le nombre de cases accessibles depuis la position (x,y)
	//découpe le plateau en blocs (chaque bloc contient le nombre de cases accessibles
		//à l'intérieur du bloc)
	//en entrée, on a une case qui est directement adjacente à la position réelle
	//****
	//bloc inaccédé à 0 (ou inaccessible)
	//valeur dans les bloc = nb de cases accessibles
	//case comptée à 2
	//case de démarrage pour un bloc à 1
	//case non traitée à 0
	private int nbCaseDispo(int x, int y){
		int xbloc = (4*(x+1)-1)/nbLig;
		int ybloc = (4*(y+1)-1)/nbLig;
		int nb = 0;

		int[][] blocs = new int[4][4];
		int[][] visited = new int[nbLig][nbCol];

		boolean isThereNoAccessibleBlock = false;
		boolean areBlockAccessibleFrom;
		
		if (!mCore.isBlocked(x, y)){ // si la case d'entree est libre on cherche

			visited[x][y] = 1;
			blocs[xbloc][ybloc] = 1; //une case accessible

			while(!isThereNoAccessibleBlock) {
				isThereNoAccessibleBlock = true;

				for (int l = 0; l < 4; l++) {
					for (int c = 0; c < 4; c++) {

						if (blocs[l][c] != 0) {//si le bloc est accessible
							areBlockAccessibleFrom = visiteBloc(blocs, l, c, visited);
							if (areBlockAccessibleFrom) isThereNoAccessibleBlock = false;
						}
					}
				}
			}
		}
		
		for (int i = 0; i < 4; i++){
			for (int j = 0; j < 4; j++){
				nb += blocs[i][j];
			}
		}
		
		return nb;
	}

	//peut etre utilisée plus tard
/*	private boolean closeToPlayer(AController player){
		boolean proche = false;
		
		for (int x = mPosition.x - 50; x < mPosition.x + 51; x++){
			if (player.getPosition().x == x){
				for (int y = mPosition.y - 51; y < mPosition.y + 51; y++){
					if (player.getPosition().y == y){
						proche = true;
					}
				}
			}
		}
		
		return proche;
	}
*/
	
}
