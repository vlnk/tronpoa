package tron.controller;

import tron.CCore;

import java.awt.*;

public class CPlayer extends AController {
    
    public CPlayer(CCore core, Point initializedPosition, Color color, String name) {
        super(core, initializedPosition, color, name);
    }
    
    @Override
    public boolean updateMove() {
        if (isDead()) return false;

        mMove = mCore.getInputDirection();

        switch (mMove) {
            case UP:
                if(mLastDirection != DIRECTIONS.DOWN){ // si pas demi tour
                    mPosition.y--;
                    mLastDirection = DIRECTIONS.UP;
                }
                else{//on continue le mouvement d'avant
                    mPosition.y++;
                }
                break;
            case DOWN:
                if(mLastDirection != DIRECTIONS.UP){
                    mPosition.y++;
                    mLastDirection = DIRECTIONS.DOWN;
                }
                else{//on continue le mouvement d'avant
                    mPosition.y--;
                }
                break;
            case LEFT:
                if(mLastDirection != DIRECTIONS.RIGHT){
                    mPosition.x--;
                    mLastDirection = DIRECTIONS.LEFT;
                }
                else{//on continue le mouvement d'avant
                    mPosition.x++;
                }
                break;
            case RIGHT:
                if(mLastDirection != DIRECTIONS.LEFT){
                    mPosition.x++;
                    mLastDirection = DIRECTIONS.RIGHT;
                }
                else{//on continue le mouvement d'avant
                    mPosition.x--;
                }
                break;
            default:
                break;
        }

        return true;
    }
}
