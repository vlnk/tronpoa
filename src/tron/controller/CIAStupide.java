package tron.controller;

import java.awt.*;

import tron.CCore;

public class CIAStupide extends AController {
	
	public CIAStupide(CCore core, Point position, Color color, String name){
		super(core, position, color, name);
		
		nbLig = mCore.getHauteur();
		nbCol = mCore.getLargeur();
        mMove = DIRECTIONS.LEFT;
	}
}
